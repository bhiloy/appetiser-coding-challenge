object TestLibs {
    //Test Libs
    val androidXJUnit = "androidx.test.ext:junit:${Versions.androidXJunitVersion}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    val junit = "junit:junit:${Versions.junitVersion}"
}