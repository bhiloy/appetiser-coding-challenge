package com.codingchallenge.myplaylist.module.local

import com.codingchallenge.myplaylist.module.local.database.SearchResultDao
import com.codingchallenge.myplaylist.module.local.model.SearchResultCacheEntity


class SearchResultDaoServiceImpl
constructor(
    private val searchResultDao: SearchResultDao
): SearchResultDaoService {

    override suspend fun insert(searchResultCacheEntity: SearchResultCacheEntity) {
        searchResultDao.insert(searchResultCacheEntity)
    }

    override suspend fun get(): List<SearchResultCacheEntity> {
        return searchResultDao.get()
    }
}