package com.codingchallenge.myplaylist.module.local

import android.content.Context
import androidx.room.Room
import com.codingchallenge.myplaylist.module.local.database.RecentPicksDao
import com.codingchallenge.myplaylist.module.local.database.SearchResultDao
import com.codingchallenge.myplaylist.module.local.database.SearchResultDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideSearchResultDb(@ApplicationContext context: Context): SearchResultDatabase {
        return Room
            .databaseBuilder(
                context,
                SearchResultDatabase::class.java,
                SearchResultDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideSearchResultDAO(searchResultDatabase: SearchResultDatabase): SearchResultDao {
        return searchResultDatabase.searchResultDao()
    }

    @Singleton
    @Provides
    fun provideRecentPicksDAO(searchResultDatabase: SearchResultDatabase): RecentPicksDao {
        return searchResultDatabase.recentPicksDao()
    }

    @Singleton
    @Provides
    fun provideSearchResultDaoService(
        searchResultDao: SearchResultDao
    ): SearchResultDaoService {
        return SearchResultDaoServiceImpl(searchResultDao)
    }

    @Singleton
    @Provides
    fun provideRecentPicksDaoService(
            recentPicksDao: RecentPicksDao
    ): RecentPicksDaoService {
        return RecentPicksDaoServiceImpl(recentPicksDao)
    }

}

























