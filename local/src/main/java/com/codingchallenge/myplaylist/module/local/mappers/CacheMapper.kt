package com.codingchallenge.myplaylist.module.local.mappers

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.domain.util.EntityMapper
import com.codingchallenge.myplaylist.module.local.model.SearchResultCacheEntity
import javax.inject.Inject

class CacheMapper
@Inject
constructor(): EntityMapper<SearchResultCacheEntity, SearchResultList> {
    override fun mapFromEntity(entity: SearchResultCacheEntity): SearchResultList {
        return SearchResultList(
            wrapperType = entity.wrapperType,
            kind = entity.kind,
            artistId = entity.artistId,
            collectionId = entity.collectionId,
            trackId = entity.trackId,
            artistName = entity.artistName,
            collectionName = entity.collectionName,
            trackName = entity.trackName,
            collectionCensoredName = entity.collectionCensoredName,
            trackCensoredName = entity.trackCensoredName,
            artworkUrl100 = entity.artworkUrl100,
            collectionPrice = entity.collectionPrice,
            trackPrice = entity.trackPrice,
            releaseDate = entity.releaseDate,
            collectionExplicitness = entity.collectionExplicitness,
            trackExplicitness = entity.trackExplicitness,
            discCount = entity.discCount,
            discNumber = entity.discNumber,
            trackCount = entity.trackCount,
            trackNumber = entity.trackNumber,
            trackTimeMillis = entity.trackTimeMillis,
            country = entity.country,
            currency = entity.currency,
            primaryGenreName = entity.primaryGenreName,
            description = entity.description,
            longDescription = entity.longDescription
        )
    }

    override fun mapToEntity(domainModel: SearchResultList): SearchResultCacheEntity {
        return SearchResultCacheEntity(
            wrapperType = domainModel.wrapperType.toString(),
            kind = domainModel.kind.toString(),
            artistId = domainModel.artistId ?: 0,
            collectionId = domainModel.collectionId ?: 0,
            trackId = domainModel.trackId ?: 0,
            artistName = domainModel.artistName.toString(),
            collectionName = domainModel.collectionName.toString(),
            trackName = domainModel.trackName.toString(),
            collectionCensoredName = domainModel.collectionCensoredName.toString(),
            trackCensoredName = domainModel.trackCensoredName.toString(),
            artworkUrl100 = domainModel.artworkUrl100.toString(),
            collectionPrice = domainModel.collectionPrice ?: 0.0,
            trackPrice = domainModel.trackPrice ?: 0.0,
            releaseDate = domainModel.releaseDate.toString(),
            collectionExplicitness = domainModel.collectionExplicitness.toString(),
            trackExplicitness = domainModel.trackExplicitness.toString(),
            discCount = domainModel.discCount ?: 0,
            discNumber = domainModel.discNumber ?: 0,
            trackCount = domainModel.trackCount ?: 0,
            trackNumber = domainModel.trackNumber ?: 0,
            trackTimeMillis = domainModel.trackTimeMillis ?: 0,
            country = domainModel.country.toString(),
            currency = domainModel.currency.toString(),
            primaryGenreName = domainModel.primaryGenreName.toString(),
            description = domainModel.description.toString(),
            longDescription = domainModel.longDescription.toString()
        )
    }

    fun mapFromEntityList(entities: List<SearchResultCacheEntity>): List<SearchResultList>{
        return entities.map {mapFromEntity(it)}
    }
}