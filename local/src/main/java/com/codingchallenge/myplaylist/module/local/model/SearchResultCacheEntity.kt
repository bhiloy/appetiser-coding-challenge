package com.codingchallenge.myplaylist.module.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_result", primaryKeys = ["artist_id","track_id"])
class SearchResultCacheEntity (
        @ColumnInfo(name = "wrapper_type") var wrapperType : String,
        @ColumnInfo(name = "kind") var kind : String,
        @ColumnInfo(name = "artist_id") var artistId : Int,
        @ColumnInfo(name = "collection_id") var collectionId : Int,
        @ColumnInfo(name = "track_id") var trackId : Int,
        @ColumnInfo(name = "artist_name") var artistName : String,
        @ColumnInfo(name = "collection_name") var collectionName : String,
        @ColumnInfo(name = "track_name") var trackName : String,
        @ColumnInfo(name = "collection_censored_name") var collectionCensoredName : String,
        @ColumnInfo(name = "track_censored_name") var trackCensoredName : String,
        @ColumnInfo(name = "artwork_url_100") var artworkUrl100 : String,
        @ColumnInfo(name = "collection_price") var collectionPrice : Double,
        @ColumnInfo(name = "track_price") var trackPrice : Double,
        @ColumnInfo(name = "release_date") var releaseDate : String,
        @ColumnInfo(name = "collection_explicitness") var collectionExplicitness : String,
        @ColumnInfo(name = "track_explicitness") var trackExplicitness : String,
        @ColumnInfo(name = "disc_count") var discCount : Int,
        @ColumnInfo(name = "disc_number") var discNumber : Int,
        @ColumnInfo(name = "track_count") var trackCount : Int,
        @ColumnInfo(name = "track_number") var trackNumber : Int,
        @ColumnInfo(name = "track_time_millis") var trackTimeMillis : Int,
        @ColumnInfo(name = "country") var country : String,
        @ColumnInfo(name = "currency") var currency : String,
        @ColumnInfo(name = "primary_genre_name") var primaryGenreName : String,
        @ColumnInfo(name = "description") var description : String,
        @ColumnInfo(name = "long_description") var longDescription : String,
)