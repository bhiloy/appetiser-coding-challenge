package com.codingchallenge.myplaylist.module.local

import com.codingchallenge.myplaylist.module.local.model.SearchResultCacheEntity


interface SearchResultDaoService {
    suspend fun insert(searchResultEntity: SearchResultCacheEntity)
    suspend fun get(): List<SearchResultCacheEntity>
}