package com.codingchallenge.myplaylist.module.data.cache

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList

interface CacheDataSourceRecentPicks {
    suspend fun insert(searchResultList: SearchResultList)
    suspend fun getAudiobooks(): List<SearchResultList>
    suspend fun getTracks(): List<SearchResultList>
}