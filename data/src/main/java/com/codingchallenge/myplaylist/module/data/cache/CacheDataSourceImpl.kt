package com.codingchallenge.myplaylist.module.data.cache

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.local.SearchResultDaoService
import com.codingchallenge.myplaylist.module.local.mappers.CacheMapper

class CacheDataSourceImpl
constructor(
    private val searchResultDaoService: SearchResultDaoService,
    private val cacheMapper: CacheMapper
): CacheDataSource {

    override suspend fun insert(searchResultList: SearchResultList) {
        searchResultDaoService.insert(cacheMapper.mapToEntity(searchResultList))
    }

    override suspend fun insertList(searchResultList: List<SearchResultList>) {
        for(item in searchResultList){
            searchResultDaoService.insert(cacheMapper.mapToEntity(item))
        }
    }

    override suspend fun get(): List<SearchResultList> {
        return cacheMapper.mapFromEntityList(searchResultDaoService.get())
    }
}