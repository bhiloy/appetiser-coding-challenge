package com.codingchallenge.myplaylist.module.data.cache

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList

interface CacheDataSource {
    suspend fun insert(searchResultList: SearchResultList)
    suspend fun insertList(searchResultList: List<SearchResultList>)
    suspend fun get(): List<SearchResultList>
}