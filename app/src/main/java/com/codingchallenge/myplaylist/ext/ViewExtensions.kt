package com.codingchallenge.myplaylist.ext

import android.os.SystemClock
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.codingchallenge.myplaylist.R

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visibleIf(isVisible: Boolean) {
    visibility = if(isVisible)
        View.VISIBLE
    else
        View.GONE
}

fun ImageView.loadItemImage(uri: String?){
    val options = RequestOptions()
        .dontAnimate()
        .placeholder(R.drawable.placeholder)
    /*.error(R.drawable.no_photo_available)*/
    Glide.with(this.context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .dontAnimate()
        .fitCenter()
        .transform(CenterCrop(), RoundedCorners(24))
        .into(this)
}

fun View.clickWithDebounce(debounceTime: Long = 1000L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action()

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}
