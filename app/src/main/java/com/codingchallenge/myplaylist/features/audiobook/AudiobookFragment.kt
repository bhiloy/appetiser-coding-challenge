package com.codingchallenge.myplaylist.features.audiobook

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.codingchallenge.myplaylist.features.main.DataState
import com.codingchallenge.myplaylist.databinding.FragmentAudiobookBinding
import com.codingchallenge.myplaylist.ext.gone
import com.codingchallenge.myplaylist.ext.visible
import com.codingchallenge.myplaylist.ext.visibleIf
import com.codingchallenge.myplaylist.features.main.MainFragmentDirections
import com.codingchallenge.myplaylist.features.main.searchResult
import com.codingchallenge.myplaylist.framework.presentation.MainStateEvent
import com.codingchallenge.myplaylist.framework.presentation.MainViewModel
import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AudiobookFragment : Fragment() {

    private lateinit var binding: FragmentAudiobookBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentAudiobookBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeObservers()
        viewModel.setStateEvent(MainStateEvent.GetAllSearchResultEvent)
        viewModel.setStateEvent(MainStateEvent.GetAudiobooksCacheEvent)

    }

    private fun subscribeObservers(){
        viewModel.dataState.observe(viewLifecycleOwner, { dataState ->
            when(dataState){
                is DataState.Success<List<SearchResultList>> -> {
                    binding.progressBar.gone()
                    appendAudiobookList(dataState.data)
                }
                is DataState.Error -> {
                    binding.progressBar.gone()
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    binding.progressBar.visible()
                }
            }
        })
        viewModel.dataStateCache.observe(viewLifecycleOwner, { dataStateCache ->
            when(dataStateCache){
                is DataState.Success<List<SearchResultList>> -> {
                    appendRecentList(dataStateCache.data)
                    binding.txtRecentPicks.visibleIf(!dataStateCache.data.isNullOrEmpty())
                }
                is DataState.Error -> {
                    displayError(dataStateCache.exception.message)
                }
                DataState.Loading -> TODO()
            }
        })
    }

    private fun displayError(message: String?){
        val msg = message ?: "Unknown error."
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    private fun appendAudiobookList(searchResultList: List<SearchResultList>){
        with(binding) {

            rvAudioBooks.withModels {
                var i = 0L
                searchResultList.filter { it.wrapperType == "audiobook" }.forEach {
                    searchResult {
                        id(i++)
                        searchResultList(it)
                        onItemSelectListener { itemSelected ->
                            viewModel.assignSearchResultItem(itemSelected)
                            viewModel.setStateEvent(MainStateEvent.SavePlayListEvent)
                            //viewModel.setStateEvent(MainStateEvent.GetAllSearchResultEvent)
                            viewModel.setStateEvent(MainStateEvent.GetAudiobooksCacheEvent)
                            findNavController().navigate(
                                MainFragmentDirections.toPlayListDetails(
                                    itemSelected
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    private fun appendRecentList(recentSearchResultList: List<SearchResultList>){
        binding.rvRecentItem.withModels {
            var i = 0L
            recentSearchResultList.forEach {
                searchResult {
                    id(i++)
                    searchResultList(it)
                    onItemSelectListener { itemSelected ->
                        findNavController().navigate(
                            MainFragmentDirections.toPlayListDetails(
                                itemSelected
                            )
                        )
                    }
                }
            }
        }
    }
}