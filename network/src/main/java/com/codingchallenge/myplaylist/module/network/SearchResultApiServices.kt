package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.network.model.SearchResultEntity
import retrofit2.http.GET

interface SearchResultApiServices {

    @GET("search?term=star&amp;country=au&amp;media=movie&ampall")
    suspend fun getData(): SearchResultEntity
}