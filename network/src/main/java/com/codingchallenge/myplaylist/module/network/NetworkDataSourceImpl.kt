package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.network.SearchResultRemoteSource
import com.codingchallenge.myplaylist.module.network.mappers.SearchResultMapper

class NetworkDataSourceImpl
constructor(
    private val searchResultRemoteSource: SearchResultRemoteSource,
    private val searchResultMapper: SearchResultMapper
): NetworkDataSource{

    override suspend fun get(): List<SearchResultList> {
        return searchResultMapper.mapFromEntityList(searchResultRemoteSource.get())
    }
}